# BBRIC::Family-Companion #

Code is now availabe @ https://forgemia.inra.fr/baric/family-companion

# Publication
<div class="alert alert-info">
Ludovic Cottret, Corinne Rancurel, Martial Briand, Sebastien Carrere
<footer><cite>BioArxiv</cite>, doi: <a target="_blank" href="https://doi.org/10.1101/266742">https://doi.org/10.1101/266742</a></footer>
</div>


# AUTHORS

* sebastien.carrere@inrae.fr
* ludovic.cottret@inrae.fr
* martial.briand@inrae.fr
* corinne.rancurel@inrae.fr


